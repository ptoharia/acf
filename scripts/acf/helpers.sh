
backup_file_if_exists ()
{
    file=$1
    buFile=$2

    if [ -f $1 ]; then
        echo "🔔 Backup existing file:"\
             $file" --> "$buFile
        mkdir -p $(dirname $2)
        cp -L $file $buFile
        rm $file
    fi

}
