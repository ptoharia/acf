#!/usr/bin/env bash

set -euo pipefail


# Ask for the administrator password upfront
#sudo -v

# Keep-alive: update existing `sudo` time stamp until `.macos` has finished
#while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &


## Install packages
if [ "$(uname)" == "Darwin" ]; then
    # Do something under Mac OS X platform
    echo "-- Dawrin detected"
elif [ "$(uname)" == "Linux" ]; then
    echo "Linux"
    if [[ $(lsb_release -i) == *"Ubuntu"* ]]; then
        echo "-- Ubuntu detected"
        # sudo apt update
        # sudo apt install cmake 
    fi

fi


ACF_DFS=${ACF_PATH}/dotfiles

source ${ACF_PATH}/scripts/acf/helpers.sh


if [ -d ${ACF_PATH}/dotfiles ]; then
    if [ -d ${ACF_PATH}/dotfiles/.git ]; then
        echo 🔁 Updating dotfiles repo
        cd ${ACF_PATH}/dotfiles
        git pull
        cd -
        echo ✅ Done
    fi
else 
    echo 🔁 Cloning dotfiles repo
    read -rp "Enter your acf dotfiles repository? " ACF_DOTFILES_REPOSITORY
    git clone --recursive ${ACF_DOTFILES_REPOSITORY} ${ACF_DFS}
    echo ✅ Done
fi


ACF_DFS_BU=${ACF_PATH}/dotfiles.bak

BU_TS=$(date '+%y%m%d-%H%M%S')

#-- GIT ---------------------------------------------------
if [ -d ${ACF_DFS}/git ]; then
    
    DOTFILE_APP=git
    DOTFILE=.gitconfig
    
    if [ -f ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ]; then
        backup_file_if_exists "${HOME}/${DOTFILE}" \
                              "${ACF_DFS_BU}/${DOTFILE_APP}/${DOTFILE}.${BU_TS}"
        ln -s ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ${HOME}/${DOTFILE} 
    fi
    
fi

#-- TMUX ---------------------------------------------------
if [ -d ${ACF_DFS}/tmux ]; then
    
    DOTFILE_APP=tmux
    DOTFILE=.tmux.conf
    
    if [ -f ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ]; then
        backup_file_if_exists "${HOME}/${DOTFILE}" \
                              "${ACF_DFS_BU}/${DOTFILE_APP}/${DOTFILE}.${BU_TS}"
        ln -s ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ${HOME}/${DOTFILE} 
    fi
fi

#-- EMACS --------------------------------------------------
if [ -d ${ACF_DFS}/emacs ]; then
    DOTDIR_APP=emacs
    DOTDIR=.emacs.d
    
    if [ -d ${ACF_DFS}/${DOTDIR_APP}/${DOTDIR} ]; then
        if [ -d ${HOME}/${DOTDIR} ]; then
            echo "🔔 Backup existing directory: " \
                 "${HOME}/${DOTDIR} --> " \
                 "${ACF_DFS_BU}/${DOTDIR_APP}/${DOTDIR}.${BU_TS}"
            
            mkdir -p ${ACF_DFS_BU}/${DOTDIR_APP}
            cp -LR ${HOME}/${DOTDIR} ${ACF_DFS_BU}/${DOTDIR_APP}/${DOTDIR}.${BU_TS}
            rm -rf ${HOME}/${DOTDIR}
        fi
        ln -s ${ACF_DFS}/${DOTDIR_APP}/${DOTDIR} ${HOME}/${DOTDIR} 
    fi
    
fi

#-- BASH --------------------------------------------------
for SHELL in bash zsh; do
    if [ -d ${ACF_DFS}/${SHELL} ]; then
        if [ -f ${ACF_DFS}/${SHELL}/$(uname)/.${SHELL}rc ]; then
            echo "🔁 Updating ${SHELL}" 
            touch ${HOME}/.${SHELL}rc
            set +e
            grep -q "source ${ACF_DFS}/${SHELL}/$(uname)/.${SHELL}rc" ${HOME}/.${SHELL}rc
            if [ $? -eq 1 ]; then
                echo "source ${ACF_DFS}/${SHELL}/$(uname)/.${SHELL}rc" >> ${HOME}/.${SHELL}rc
            fi
        fi
        if [ -f ${ACF_DFS}/${SHELL}/setup.sh ]; then
            echo "🔁 Running custom config for" $SHELL
            /usr/bin/env bash ${ACF_DFS}/${SHELL}/setup.sh
        fi
    fi
done

if [ -d ${ACF_DFS}/ssh ]; then
    
    DOTFILE_APP=ssh
    DOTFILEDIR=.ssh
    DOTFILE=config
    
    if [ -f ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ]; then
        if [ -f ${HOME}/${DOTFILEDIR}/${DOTFILE} ]; then
            echo "🔔 Backup existing file: " \
                 "${HOME}/${DOTFILEDIR}/${DOTFILE} --> " \
                 "${ACF_DFS_BU}/${DOTFILE_APP}/${DOTFILE}.${BU_TS}"
            mkdir -p ${ACF_DFS_BU}/${DOTFILE_APP}
            cp -L ${HOME}/${DOTFILEDIR}/${DOTFILE} ${ACF_DFS_BU}/${DOTFILE_APP}/${DOTFILE}.${BU_TS}
            rm ${HOME}/${DOTFILEDIR}/${DOTFILE}
        fi
        ln -s ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE} ${HOME}/${DOTFILEDIR}/${DOTFILE}
        chmod 600 ${ACF_DFS}/${DOTFILE_APP}/${DOTFILE}
    fi
fi


if [ "$(uname)" = "Darwin" ]; then
    backup_file_if_exists "${HOME}/Library/Preferences/com.apple.Terminal.plist" \
                          "${ACF_DFS_BU}/macos/com.apple.Terminal.plist.${BU_TS}"
    ln -s ${ACF_DFS}/macos/com.apple.Terminal.plist \
       ${HOME}/Library/Preferences/com.apple.Terminal.plist
fi


echo ✅ All done 🤘
