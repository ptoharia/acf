#!/usr/bin/env bash

set -euo pipefail


echo "ACF - Automatic Config Files"
echo "----------------------------"
export ACF_PATH="$HOME/.acf"
echo "-- Cloning acf into : ${ACF_PATH}"

if [ -d ${ACF_PATH} ]; then
    echo "-- (ERROR) Directory ${ACF_PATH} already exists"
    exit
fi

git --version

git clone --recursive https://gitlab.com/ptoharia/acf.git ${ACF_PATH}

${ACF_PATH}/scripts/acf/install.sh
